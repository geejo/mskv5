package com.piserve.geejo.mskv4.myservicekart.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.piserve.geejo.mskv4.R;
import com.piserve.geejo.mskv4.myservicekart.network.VolleySingleton;

/**
 * Created by droidspace on 25/5/15.
 */
public class MyFragment extends Fragment{

        private TextView textView;

        public static MyFragment getInstance(int position) {
            MyFragment myFragment = new MyFragment();
            Bundle bundleArgs = new Bundle();

            bundleArgs.putInt("position", position);
            myFragment.setArguments(bundleArgs);
            return myFragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View layout = inflater.inflate(R.layout.fragment_my, container, false);
            textView = (TextView) layout.findViewById(R.id.position);
            Bundle bundleValues = getArguments();
            if(bundleValues!=null){
                textView.setText("The page currently selected is" + bundleValues.getInt("position"));
            }

            //-----------------NETWORKING CODE---------------//

            RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();

            StringRequest request = new StringRequest(Request.Method.GET, "http://php.net/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
//                    Toast.makeText(getActivity(), "RESPONSE: "+response, Toast.LENGTH_SHORT).show();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                    Toast.makeText(getActivity(), "ERROR: "+error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            requestQueue.add(request);
            return layout;
        }
}
