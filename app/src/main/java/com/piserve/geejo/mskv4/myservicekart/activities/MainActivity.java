package com.piserve.geejo.mskv4.myservicekart.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.piserve.geejo.mskv4.R;
import com.piserve.geejo.mskv4.myservicekart.fragments.FragmentAll;
import com.piserve.geejo.mskv4.myservicekart.fragments.FragmentFeatured;
import com.piserve.geejo.mskv4.myservicekart.fragments.FragmentNew;
import com.piserve.geejo.mskv4.myservicekart.fragments.MyFragment;
import com.piserve.geejo.mskv4.myservicekart.fragments.NavigationDrawerFragment;

import java.util.List;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;


public class MainActivity extends ActionBarActivity implements MaterialTabListener{

    private Toolbar mToolbar;
    private NavigationDrawerFragment mDrawerFragment;
    private DrawerLayout mDrawerLayout;

    private MaterialTabHost mTabHost;
    private ViewPager mViewPager;

    public static final int SERVICES_ALL = 0;
    public static final int SERVICES_FEATURED = 1;
    public static final int SERVICES_NEW = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_appbar);

        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        mDrawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, mToolbar);

        mTabHost = (MaterialTabHost) findViewById(R.id.materialTabHost);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);

        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                mTabHost.setSelectedNavigationItem(position);
            }
        });

        for(int i=0; i<adapter.getCount(); i++ ){
            mTabHost.addTab(
                    mTabHost.newTab().
                            setText(adapter.getPageTitle(i)).
                            setTabListener(this)
            );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Toast.makeText(this, "Hey you just hit " + item.getTitle(), Toast.LENGTH_LONG).show();
            return true;
        }

        if (id == R.id.action_next) {
            startActivity(new Intent(this, DetailActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(MaterialTab materialTab) {
        mViewPager.setCurrentItem(materialTab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {

    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter{
        FragmentManager fragmentManager;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentManager = fm;
        }

        @Override
        public Fragment getItem(int num) {
            Fragment fragment = null;
            switch (num){
                case SERVICES_ALL:
                    fragment = FragmentAll.newInstance("","");
                    break;
                case SERVICES_FEATURED:
                    fragment = FragmentFeatured.newInstance("","");
                    break;
                case SERVICES_NEW:
                    fragment = FragmentNew.newInstance("","");
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        public CharSequence getPageTitle(int position){
            return getResources().getStringArray(R.array.tabs)[position];
        }
    }
}
