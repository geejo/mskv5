package com.piserve.geejo.mskv4.myservicekart.extras;

/**
 * Created by Droid Space on 5/28/2015.
 */
public class UrlEndPoints {
    public static final String URL_SERVICE_BASE = "http://myservicekart.com/public/";
    public static final String URL_SEARCH_BASE = "http://myservicekart.com/public/search";
    public static final String URL_CHAR_QUESTION = "?";
    public static final String URL_CHAR_AMPERSAND = "&";
    public static final String URL_PARM_SEARCH = "search=";
    public static final String URL_PARM_PAGENO = "pageno=";
}
