package com.piserve.geejo.mskv4.myservicekart.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.piserve.geejo.mskv4.R;
import com.piserve.geejo.mskv4.myservicekart.activities.DetailActivity;
import com.piserve.geejo.mskv4.myservicekart.pojo.RowInfo;

import java.util.Collections;
import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {

    private Context context;
    private LayoutInflater mInflater;
    List<RowInfo> mListData = Collections.emptyList();

    public ServiceAdapter(Context context, List<RowInfo> listData) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mListData = listData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_info, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        RowInfo currentRowInfo = mListData.get(position);
        holder.titleTextView.setText(currentRowInfo.title);
        holder.iconImageView.setImageResource(currentRowInfo.iconId);

    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView titleTextView;
        ImageView iconImageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            titleTextView = (TextView) itemView.findViewById(R.id.text_view_list);
            iconImageView = (ImageView) itemView.findViewById(R.id.image_view_list);
        }

        @Override
        public void onClick(View v) {
            Intent detailIntent = new Intent(context, DetailActivity.class);
            context.startActivity(detailIntent);
        }
    }
}
