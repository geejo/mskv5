package com.piserve.geejo.mskv4.myservicekart.extras;

/**
 * Created by Droid Space on 5/28/2015.
 */
public class Service {
    private long id;
    private String name;
    private String orgName;
    private int rating;
    private long price;
    private String imageUrl;

    public Service() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice(long price) {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String toString(){
        return "ID: " +id+
                "Name: " +name+
                "Price: " +price+
                "OrgName: " +orgName+
                "Rating: " +rating+
                "ImageUrl: " +imageUrl;

    }
}
