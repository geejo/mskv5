package com.piserve.geejo.mskv4.myservicekart.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;

import com.piserve.geejo.mskv4.R;
import com.piserve.geejo.mskv4.myservicekart.pojo.RowInfo;
import com.piserve.geejo.mskv4.myservicekart.adapters.ServiceAdapter;

import java.util.ArrayList;
import java.util.List;

public class NavigationDrawerFragment extends Fragment {

    public static final String PREF_FILE_NAME = "testpref";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer"; //if reading or saving to preference we need a KEY

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private boolean mUserLearnedDrawer; //if user is aware of Drawer's existance or not. Store this value in a SharedPreferences file, and to check if the Drawer has been opened for first time or not. If it's saved, and we can get a value and we will display the drawer
    private boolean mFromSavedInstanceState; //if it is coming back from rotation

    private View mContainerView;
    private RecyclerView mRecyclerView;
    private ServiceAdapter mServiceAdapter;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String preferencePair = readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"); //false - user never opened Drawer
        mUserLearnedDrawer = (Boolean.valueOf(preferencePair));

        if (savedInstanceState != null) {
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mRecyclerView = (RecyclerView) layout.findViewById(R.id.drawer_list);

        mServiceAdapter = new ServiceAdapter(getActivity(), getData());
        mRecyclerView.setAdapter(mServiceAdapter);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return layout;
    }

    public static List<RowInfo> getData() {
        List<RowInfo> rowInfoList = new ArrayList<>();
        int[] icons = {R.drawable.ic_action_search, R.drawable.ic_action_bad, R.drawable.ic_action_favorite, R.drawable.ic_action_not_important};
        String[] titles = {"Search", "Bad", "Favorite", "Unimportant"};
        for (int i = 0; i < 100; i++) {
            RowInfo rowInfo = new RowInfo();
            rowInfo.iconId = icons[i%icons.length];
            rowInfo.title = titles[i%titles.length];
            rowInfoList.add(rowInfo);
        }
        return rowInfoList;
    }

    public void setUp(int fragmentId, DrawerLayout drawerlayout, final Toolbar toolbar) {
        mContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerlayout;

        mDrawerToggle = new ActionBarDrawerToggle
                (getActivity(), drawerlayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer + ""); //mUserLearnedDrawer - boolean to String
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                if (slideOffset < 0.6) {
                    toolbar.setAlpha(1 - slideOffset);
                }
            }
        };

        //If user never seen drawer, and if fragment started for first time
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mContainerView);
        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply(); //commit
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

}
