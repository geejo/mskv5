package com.piserve.geejo.mskv4.myservicekart.pojo;

public class RowInfo {
    public int iconId;
    public String title;

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
