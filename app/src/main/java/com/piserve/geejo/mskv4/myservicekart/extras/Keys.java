package com.piserve.geejo.mskv4.myservicekart.extras;

/**
 * Created by Droid Space on 5/28/2015.
 */
public class Keys {
    public interface EndPointServicesHome{
        public static final String KEY_ID = "id";
        public static final String KEY_NAME = "name";
        public static final String KEY_PRICE = "price";
        public static final String KEY_ORG_NAME = "organizationName";
        public static final String KEY_RATING = "rating";
        public static final String KEY_IMAGE_THUMBNAIL = "smallPicture";
        public static final String KEY_POSTED_USER = "postedUser";
        public static final String KEY_PROFILE = "profile";
    }
}
