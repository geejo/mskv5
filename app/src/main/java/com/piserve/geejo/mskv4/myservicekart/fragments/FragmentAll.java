package com.piserve.geejo.mskv4.myservicekart.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.piserve.geejo.mskv4.R;
import com.piserve.geejo.mskv4.myservicekart.extras.Service;
import com.piserve.geejo.mskv4.myservicekart.logging.L;
import com.piserve.geejo.mskv4.myservicekart.myservicekart.MyApplication;
import com.piserve.geejo.mskv4.myservicekart.network.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.piserve.geejo.mskv4.myservicekart.extras.UrlEndPoints.*;
import java.util.ArrayList;

import static com.piserve.geejo.mskv4.myservicekart.extras.Keys.EndPointServicesHome.*;


public class FragmentAll extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    private ArrayList<Service> listServices = new ArrayList<>();

    public static final String SERVICE_BASE_URL = "http://myservicekart.com/public/";
    private static final String SEARCH_BASE_URL = "http://myservicekart.com/public/search";

    // TODO: Rename and change types and number of parameters
    public static FragmentAll newInstance(String param1, String param2) {
        FragmentAll fragment = new FragmentAll();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static String getRequestUrl() {
        return URL_SEARCH_BASE
                + URL_CHAR_QUESTION
                + URL_PARM_SEARCH
                + URL_CHAR_AMPERSAND
                + URL_PARM_PAGENO + MyApplication.HOME_PAGE_NUMBER;
    }

    public FragmentAll() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();

        sendJsonRequest();
    }

    public void sendJsonRequest(){

        JsonArrayRequest request = new JsonArrayRequest
                (
                        getRequestUrl(),
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                parseJSONResponse(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );
        mRequestQueue.add(request);
    }

    private void parseJSONResponse(JSONArray response) {
        if (response == null && response.length() == 0) {
            return;
        }
        try {
            // TODO: Remove testing part
//            StringBuilder data = new StringBuilder();

            for (int i = 0; i < response.length(); i++) {
                JSONObject serviceObject = response.getJSONObject(i);
                long id = serviceObject.getLong(KEY_ID);
                String name = serviceObject.getString(KEY_NAME);
                long price = serviceObject.getLong(KEY_PRICE);

                String smallPictureUrl = null;
                if(serviceObject.has(KEY_IMAGE_THUMBNAIL)){
                    smallPictureUrl = serviceObject.getString(KEY_IMAGE_THUMBNAIL);
                }

                JSONObject postedUserObject = serviceObject.getJSONObject(KEY_POSTED_USER);
                JSONObject profileObject = postedUserObject.getJSONObject(KEY_PROFILE);

                String orgName = profileObject.getString(KEY_ORG_NAME);
                int rating = profileObject.getInt(KEY_RATING);

//                data.append(name + " " + id + " " + "\n");
//                L.T(getActivity(), data.toString());
                Service service = new Service();
                service.setId(id);
                service.setName(name);
                service.setPrice(price);
                service.setOrgName(orgName);
                service.setRating(rating);
                service.setImageUrl(smallPictureUrl);

                listServices.add(service);
            }
            L.t(getActivity(), listServices.toString());
        }
        catch(Exception e){

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all, container, false);
    }


}
